# Godot - CI

[![pipeline status](https://gitlab.com/dime.nicius/godot-ci/badges/master/pipeline.svg)](https://gitlab.com/dime.nicius/godot-ci/commits/master) 

Esse é um exemplo de pipeline para projetos [Godot](https://godotengine.org/) usando versionamento automático de código na branch `master`, deploy dos binários (Linux, OSX, Windows) no [Itch.io](https://itch.io/) e disponibilização da versão web no [Gitlab Pages](https://about.gitlab.com/stages-devops-lifecycle/pages/) do repositório.

Referências:
- [Godot CI](https://github.com/aBARICHELLO/godot-ci)
- [GitLab semantic versioning](https://github.com/mrooding/gitlab-semantic-versioning)
- [SemVer](https://semver.org/)

## Configuração

### Versionamento

Para que o versionamento seja feito corretamente é necessário que se configure uma `token` de acesso a API no projeto. Nesse caso foi gerado uma `token` em minha conta pessoal, mas em projetos corporativos o ideal é que se crie uma *conta de serviço* e gere o `token` nela.

Adicione como variáveis do projeto (**Settings > CI/CD > Variables**), os valores:

- **NPA_USERNAME** - nome do usuário
- **NPA_PASSWORD** - token de acesso a api

Crie e adicione as seguintes `labels` no projeto (**Issues > Labels**):

- **bumb-major**
- **bump-minor**

Essas `labels` são utilizadas nos `merge requests` para definir qual será o tipo de atualização feito pelo versionamento. Caso não seja utilizada nenhuma `tag`, o script fará o `bump` na versão de **PATCH**, no caso de uma das `labels` ser definidas o `bump` acontece na versão **MAJOR** ou **MINOR** respectivamente.

### Itch.io

Para disponibilizar o projeto no [Itch.io](https://itch.io/) e no [Gitlab Pages](https://about.gitlab.com/stages-devops-lifecycle/pages/), foi utilizado esse [repositório](https://github.com/aBARICHELLO/godot-ci) como base.

É necessário que se configure 3 variáveis para o acesso ao Itch.io:

- **ITCHIO_USERNAME** - nome do usuário
- **ITCHIO_GAME** - nome do jogo na plataforma
- **BUTLER_API_KEY** - token de acesso a api via [butler](https://itch.io/docs/butler/)

Caso não saiba os valores exatos, basta olhar URL do projeto: **https://\<username>.itch.io/\<game>**

Para gerar o `token` do [butler](https://itch.io/docs/butler/),  siga a [documentação oficial](https://itch.io/docs/butler/login.html).

## Pipeline

A criação do pipeline foi baseada no repositório: [gitlab-semantic-versioning (mrooding)](https://github.com/mrooding/gitlab-semantic-versioning)


O pipeline é composto dos `stages`:
  - env-vars
  - test
  - build
  - tag-version
  - registry


#### env
Responsável por gerar as variáveis utilizadas pelos `jobs` de versionamento e registry.

#### test
O `job` de testes é feito através do plugin [GUT](https://github.com/bitwes/Gut/).

#### build
O `job` de build é feito utilizando essa [imagem](https://github.com/aBARICHELLO/godot-ci).

#### version
Execução do script de versionamento, para mais informações sobre como o script funciona ver o [repositório de origem](https://github.com/mrooding/gitlab-semantic-versioning).

#### deploy
Assim como o `job` de **build**, o `job` de deploy é feito utilizando essa [imagem](https://github.com/aBARICHELLO/godot-ci). Nesse repositório também se encontra a opção de deploy no [GitHub Pages](https://pages.github.com/).

## Projeto de Exemplo

O código utilizado no projeto de exemplo foi pego do tutorial de [TDD e PONG](https://www.youtube.com/channel/UCkGO6guRt_5fOh3oDHbfg9w/playlists) que se encontra na documentação do [GUT](https://github.com/bitwes/Gut/wiki/Tutorials).

O jogo pode ser acessado [aqui](https://d1m3.gitlab.io/godot-ci/).